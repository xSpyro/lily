import numpy as np

class Vector2:
	def __init__(self, x = 0.0, y = 0.0):
		self.x = x
		self.y = y
		
	def toString(self):
		return "X: " + str(self.x) + " Y: " + str(self.y)

class Vector3:
	def __init__(self, x = 0.0, y = 0.0, z = 0.0):
		self.x = x
		self.y = y
		self.z = z
		
	def toString(self):
		return "X: " + str(self.x) + " Y: " + str(self.y) + " Z: " + str(self.z)

class Rectangle:
	def __init__(self, x = 0.0, y = 0.0, w = 0.0, h = 0.0):
		self.x = x
		self.y = y
		self.w = w
		self.h = h
		
	def toString(self):
		return "X: " + str(self.x) + " Y: " + str(self.y) + " W: " + str(self.w) + " H: " + str(self.h)