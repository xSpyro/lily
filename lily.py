import numpy as np
import collision
import time

from datetime import datetime

import threading

class Action:
	def __init__(self, start, update, finish):
		self.start = start
		self.update = update
		self.finish = finish
	# Signatures of functions
	# def start(self):
	# def update(self, deltaTime = 0.0):
	# def finish(self):

class EventQueue (threading.Thread):
	action_queue = []
	def __init__(self, threadID, name, running = True):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.name = name
		self.running = running
		
	def run(self):
		print("Event manager starting!")
		while self.running:
			print("Start before action!")
			if self.action_queue and self.action_queue.count() > 0:
				print("Processing action!")
				self.action_queue[0].start()
				
				# TODO: Now the thing is, the processing could be done by iteration or block thread...
				self.action_queue[0].update(deltaTime)
				self.action_queue.finish()
				del self.action_queue[0]
		
			# TODO: Let's not torch the CPU just yet...
			time.sleep(1)
				
	# TODO: Now this is a locking hazard.
	# The only place this list should be modified is from this, and in the run section.
	# It is multiple threads! Lock it ffs...
	def addAction(action):
		self.action_queue.append(action)
		
	def shutDown():
		self.running = False

class Lily:
	# TODO: Name and ID? hmm... will be just one instance, but what the hell...
	queueManager = EventQueue(1, "EventListener")
	
	def __init__(self, startFunction, updateFunction, drawFunction):
		self.startFunction = startFunction
		self.updateFunction = updateFunction
		self.drawFunction = drawFunction
	
	def run(self):
		running = True
		startTime = datetime.now()
		endTime = datetime.now()
		deltaMilliseconds = 0.0
		
		self.queueManager.start()
		
		self.startFunction(self)
		
		print("Lily framework starting!")
		while running:
			startTime = datetime.now()
			# Check Input
			# TODO: Maybe this is not a good place ...
			
			# Update
			print("Weird!")
			self.updateFunction(self, deltaMilliseconds)
			
			# Draw
			self.drawFunction(self)
			
			endTime = datetime.now()
			deltaTime = endTime - startTime
			deltaMilliseconds = eltaTime.total_seconds()
			
		# Now it's good idea to actually try to finish them.
		# TODO: Maybe there is a better way to "finalize" the events.
		self.queueManager.join()
		
	def addAction(self, action):
		print("Here is the problem!")
		#self.queueManager.append(action)	
	
		# End of Run
	def shutDown(self):
		self.queueManager.shutDown()
		running = False