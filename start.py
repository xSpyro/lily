import numpy as np
from collision import Vector2
from collision import Vector3
from collision import Rectangle
import time

# For flushing
import sys

import threading

from datetime import datetime

import threading
import time

class Entity:
	def __init__(self, position = Vector3()):
		self.position = position
	
class BoardCell:
	def __init__(self, rowIndex = 0, columnIndex = 0):
		self.rowIndex = rowIndex
		self.columnIndex = columnIndex
		self.worldPosition = Vector3(0, 0, 0)

class Board:
	def __init__(self, nrOfColumns = 10, nrOfRows = 10):
		self.nrOfColumns = nrOfColumns
		self.nrOfRows = nrOfRows
		
		self.boardStates = [[]]
		
		for x in range(0, nrOfColumns):
			self.boardStates.append([])
			for y in range(0, nrOfRows):
				self.boardStates[x].append("X")
	
	def draw(self, howToDrawFunc):
		howToDrawFunc(self.boardStates)
	
	def setStateAt(self, rowIndex, columnIndex, value = "X"):
		self.boardStates[rowIndex][columnIndex] = value
		
	def setStateAtSafe(self, rowIndex, columnIndex, value = "X"):
		if(rowIndex < 0 or rowIndex >= self.nrOfRows):
			return "Out of bounds!"
		if(columnIndex < 0 or columnIndex >= self.nrOfColumns):
			return "Out of bounds!"
		self.boardStates[rowIndex][columnIndex] = value
		return "Success!"
		
	def getState(self, rowIndex, columnIndex):
		return self.boardStates[rowIndex][columnIndex]
		
	def getStateSafe(self, rowIndex, columnIndex, outOfBoundsValue = "0"):
		if(rowIndex < 0 or rowIndex >= self.nrOfRows):
			return outOfBoundsValue
		if(columnIndex < 0 or columnIndex >= self.nrOfColumns):
			return outOfBoundsValue
		return self.boardStates[rowIndex][columnIndex]
	
class Snake (threading.Thread):
	def __init__(self, threadID, name):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.name = name
	
		self.running = True
		
		self.counter = 0.0
	
		self.body = {Entity(), Entity()}
		self.nr = 0
		
		# Read in all needed data here
		self.board = Board()
		self.board.setStateAt(3,3, "L")
		
		# TODO: Yepp, this is very... nice... :0
		self.direction = "right"
		
		self.snakeBody = []
		
		# TODO: Last part is the head!
		self.snakeBody.append(BoardCell(0, 0))
		self.snakeBody.append(BoardCell(1, 0))
		
		for bodyPart in self.snakeBody:
			print(str("Setting state at: " + str(bodyPart.rowIndex) + " " + str(bodyPart.columnIndex)))
			self.board.setStateAt(bodyPart.rowIndex, bodyPart.columnIndex, "S")
		
	def update(self, deltaTime):
		#print("Update")
		hej = ""
		
	def move(self):

		
		print("Printing indices to move around")
		
		self.board.setStateAtSafe(self.snakeBody[0].rowIndex, self.snakeBody[0].columnIndex)
		
		
		for i in range(0, len(self.snakeBody)-1):
			prev = self.snakeBody[i]
			next = self.snakeBody[i+1]
			
			prev.rowIndex = next.rowIndex
			prev.columnIndex = next.columnIndex
			
			self.board.setStateAtSafe(prev.rowIndex, prev.columnIndex, "O")
			
		if self.direction == "up":
			self.snakeBody[-1].columnIndex -= 1
			hej = ""
		elif self.direction == "down":
			self.snakeBody[-1].columnIndex += 1
			hej = ""
		elif self.direction == "left":
			self.snakeBody[-1].rowIndex -= 1
			hej = ""
		elif self.direction == "right":
			self.snakeBody[-1].rowIndex += 1
			hej = ""
		
		self.board.setStateAtSafe(self.snakeBody[-1].rowIndex, self.snakeBody[-1].columnIndex, "O")
		
	def draw(self):
		self.board.draw(self.drawBoard)
		
	# TODO: Add robot board here later
	def drawBoard(self, boardStates):
		for columnIndex, column in enumerate(boardStates):
			line = ""
			for rowIndex, row in enumerate(column):
				line += boardStates[rowIndex][columnIndex] + " "
			print(line)
		
	# TODO: These functions are not synchronized!
	def turnLeft(self):
		if self.direction == "up":
			self.direction = "left"
		elif self.direction == "down":
			self.direction = "right"
		elif self.direction == "left":
			self.direction = "down"
		elif self.direction == "right":
			self.direction = "up"
			
	def turnRight(self):
		if self.direction == "up":
			self.direction = "right"
		elif self.direction == "down":
			self.direction = "left"
		elif self.direction == "left":
			self.direction = "up"
		elif self.direction == "right":
			self.direction = "down"
	
	# Here starts the thread logic, ignore it!
	def run(self):
		self.running = True

		startTime = datetime.now()
		endTime = datetime.now()
		deltaMilliseconds = 0.0
		
		while self.running:
			startTime = datetime.now()	
			self.counter += deltaMilliseconds
			
			self.update(deltaMilliseconds)
			
			if self.counter > 1.0:
				# TODO: Here should it wait until the robot is ready with its drawing
				self.move()
				self.draw()
			
				print("Draw, per one sec: " + str(self.counter))
				sys.stdout.flush()
				self.counter = 0.0
				
			endTime = datetime.now()	
			deltaTime = endTime - startTime	
			deltaMilliseconds = deltaTime.total_seconds()	
		
	def stop(self):
		print("Stopping snake game")
		self.running = False

		
board = Board()

board.setStateAt(3,3, "L")

snake = Snake(0, "Snake thread")
snake.start()

running = True

while running:
	result = input("")

	if result == "x":
		running = False
		snake.stop()
	elif result == "left":
		snake.turnLeft()
	elif result == "right":
		snake.turnRight()

snake.join()	
	
print ("Exiting Main Thread")