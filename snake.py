import uarm_controller
import lily

class SnakeImpl:
	snakeBody = []
	mapWithStates = []
	
	uarmController = uarm_controller.UarmController()
	
	def startPrint():
		print("Start!")
	def updatePrint():
		print("Update!")
	def finishPrint():
		print("Finished!")
	
	# TODO: Now, there is ugly dependencies here, maybe solve them better...
	def __init__(self, nrOfColumns, nrOfRows):
		self.nrOfColumns = nrOfColumns
		self.nrOfRows = nrOfRows
		
	def start(self, lilyFramework):
		print("Start is getting called by framework!")
		
		self.uarmController.start()
		action = lily.Action(self.startPrint, self.updatePrint, self.finishPrint)
		lilyFramework.addAction(action)
		
	def update(self, lilyFramework, deltaTime):
		print("Update is getting called by framework!")
		
		x = input("Write x to exit...")
		if x == "x":
			lily.shutDown()
	def draw(self, lilyFramework):
		print("Draw is getting called by framework!")
		#for rows in 
		
	